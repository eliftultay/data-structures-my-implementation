package main;

public class Node <E> {
    public E value;
    public Node next;
    public Node prev;

    public Node() {
    }

    public Node(E value) {
        this.value = value;
    }
}

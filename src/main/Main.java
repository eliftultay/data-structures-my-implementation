package main;

import main.list.DoublyLinkedList;
import main.list.SinglyLinkedList;
import main.store.Queue;
import main.store.Stack;

import java.util.LinkedList;

public class Main {
    public static void main(String [] args) {
        //testSingly();
        //testDoubly();
        //testStack();
        testQueue();
    }

    public static void testSingly() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList();
        singlyLinkedList.add(3);
        singlyLinkedList.add(8);
        singlyLinkedList.add(-1);
        singlyLinkedList.add(7);
        singlyLinkedList.add(3);
        singlyLinkedList.add(15);
        singlyLinkedList.add(1);

        singlyLinkedList.printSinglyLinkedList();

        Node value = singlyLinkedList.find(-1);
        if (value != null) {
            System.out.println(value.value);
        }
        singlyLinkedList.delete(0);
        singlyLinkedList.delete(15);
        singlyLinkedList.printSinglyLinkedList();
        singlyLinkedList.delete(3);
        singlyLinkedList.printSinglyLinkedList();
    }

    public static void testDoubly() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList();
        doublyLinkedList.add(0);
        doublyLinkedList.add(8);
        doublyLinkedList.add(-1);
        doublyLinkedList.add(7);
        doublyLinkedList.add(3);
        doublyLinkedList.add(15);
        doublyLinkedList.add(1);

        doublyLinkedList.printDoublyLinkedList();

        Node value = doublyLinkedList.find(15);
        if (value != null) {
            System.out.println(value.value);
        }
        doublyLinkedList.delete(0);
        doublyLinkedList.printDoublyLinkedList();
        doublyLinkedList.delete(1);
        doublyLinkedList.printDoublyLinkedList();
        doublyLinkedList.delete(7);
        doublyLinkedList.printDoublyLinkedList();

    }

    public static void testStack() {
        Stack<String> stack = new Stack<>();

        stack.push("M");
        stack.push("U");
        stack.push("S");
        stack.push("T");
        stack.push("A");
        stack.push("F");
        stack.push("A");
        stack.push("Y");
        stack.push("U");
        stack.push("S");
        stack.push("U");
        stack.push("F");
        stack.push("*---*");

        stack.print();

        int size = stack.size();
        System.out.println("\nSize: " + size);

        Node<String> testPop = stack.pop();
        System.out.println("Pop: " + testPop.value);
        System.out.println("After Pop: " + stack.size());

        Node<String> testPeek = stack.peek();
        System.out.println("Peek: " + testPeek.value);

        stack.clear();
        System.out.println("After Clear Stack: " + stack.size());
    }

    public static void testQueue() {

        Queue<String> queue = new Queue<>();

        System.out.println("Is empty :" + queue.isEmpty());

        queue.enqueue("*---*");
        queue.enqueue("M");
        queue.enqueue("U");
        queue.enqueue("Y");
        queue.enqueue("U");
        queue.enqueue("T");
        queue.enqueue("U");
        queue.enqueue("*---*");

        queue.print();

        System.out.println("\nIs full :" + queue.isFull());

        int size = queue.size();
        System.out.println("Size: " + size);

        Node<String> testDequeue = queue.dequeue();
        System.out.println("Dequeue: " + testDequeue.value);
        System.out.println("After dequeue: " + queue.size());

        Node<String> testPeek = queue.peek();
        System.out.println("Peek: " + testPeek.value);

        queue.clear();
        System.out.println("After Clear Queue: " + queue.size());
    }

}

package main.list;

import main.Node;

public class SinglyLinkedList <E> {
    Node head;
    Node tail;

    public void add(E value) {
        Node node = new Node();
        node.value = value;

        if (head == null && tail ==null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
    }

    public void delete(E value) {
        Node temp = head;

        if (temp == null) {
            return;
        }
        if (temp.value == value) {
            head = head.next;
            return;
        }

        while(temp.next != null) {
            if(temp.next.value == value) {
                temp.next = temp.next.next;
                break;
            } else{
                temp = temp.next;
            }
        }
    }

    public Node find(E value) {
        Node temp = head;
        if (temp == null) {
            return null;
        }

        while(temp != null) {
            if(temp.value == value) {
                return temp;
            } else{
                temp = temp.next;
            }
        }
        return null;
    }

    public void printSinglyLinkedList() {
        Node temp = head;
        while(temp != null) {
            System.out.print(" " + temp.value);
            temp = temp.next;
        }
        System.out.println();

    }

}

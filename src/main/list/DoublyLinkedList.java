package main.list;

import main.Node;

public class DoublyLinkedList <E> {
    Node head;
    Node tail;

    public void add(E value) {
        Node node = new Node();
        node.value = value;

        if (head == null && tail ==null) {
            head = node;
        } else {
            tail.next = node;
            node.prev = tail;
        }
        tail = node;
    }

    public void delete(E value) {
        Node temp = head;

        if (temp == null) {
            return;
        }
        if (temp.value == value) {
            head = head.next;
            head.prev = null;
            return;
        }

        while(temp != null) {
            if(temp.value == value) {
                temp.prev.next = temp.next;
                if (temp.next != null) {
                    temp.next.prev = temp.prev;
                }
                break;
            } else{
                temp = temp.next;
            }
        }
    }

    public Node find(E value) {
        Node temp = head;
        if (temp == null) {
            return null;
        }

        while(temp != null) {
            if(temp.value == value) {
                return temp;
            } else{
                temp = temp.next;
            }
        }
        return null;
    }

    public void printDoublyLinkedList() {
        Node temp = head;
        while(temp != null) {
            System.out.print(" " + temp.value);
            temp = temp.next;
        }
        System.out.println();

    }

}

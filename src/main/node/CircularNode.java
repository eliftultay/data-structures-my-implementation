package main.node;

import main.Node;

public class CircularNode {

    public static void main(String [] args) {

        // Circular List
        Node circular1 = new Node();
        Node circular2 = new Node();

        circular1.next = circular2;
        circular2.next = circular1;

        Node temp = circular1;
        circular1.value = 99;
        circular2.value = 77;

        while(temp != null) {
            System.out.println(temp.value);
            //Node'un nextine point ediyoruz, yani node++ gibi.
            temp = temp.next;
        }
    }
}

package main.node;

import main.Node;

public class DoublyNode {

    public static void main(String [] args) {
        Node head = new Node();
        Node tail = new Node();
        head.value = 3;
        tail.value = 4;

        head.next = tail;
        head.prev = null;

        tail.prev = head;

        Node node = tail;

        while(node != null) {
            System.out.println(node.value);
            //Node'un prev'ine point ediyoruz, yani node-- gibi.
            node = node.prev;
        }
    }

}

package main.node;

import main.Node;

public class SinglyNode {

    public static void main(String [] args) {
        Node head = new Node();
        head.value = 3;

        Node node1 = new Node();
        node1.value = 4;

        head.next = node1;

        Node node = head;

        while(node != null) {
            System.out.println(node.value);
            //Node'un nextine point ediyoruz, yani node++ gibi.
            node = node.next;
        }

    }
}
